## Собрать образ с приложением
```
docker-compose -f docker-compose.yml build
```

## Запустить проект
```
docker-compose -f docker-compose.yml up
```