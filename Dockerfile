FROM node:16

ENV APP_ROOT /app
ENV PORT 3000

WORKDIR ${APP_ROOT}
ADD . ${APP_ROOT}

RUN npm install

EXPOSE ${PORT}

CMD ["npm", "run", "start:prod"]
