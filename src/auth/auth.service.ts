import { Injectable } from '@nestjs/common';
import { UserService } from '../user/user.service';
import * as bcrypt from 'bcryptjs';
import { JwtService } from '@nestjs/jwt';
import { FindUserDto } from '../user/dto/find_user.dto';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UserService,
    private jwtService: JwtService,
  ) {}

  async validateUser(login: string, pass: string): Promise<any> {
    const user = await this.usersService.findByLogin(login);
    if (user && this.passwordsAreEqual(user.password, pass)) {
      const { password, ...result } = user;
      return result;
    }
    return null;
  }

  async login(user: FindUserDto) {
    const payload = {
      second_name: user.second_name,
      first_name: user.first_name,
      sub: user.id,
    };
    return {
      token: this.jwtService.sign(payload),
    };
  }

  private passwordsAreEqual(
    hashedPassword: string,
    plainPassword: string,
  ): boolean {
    return bcrypt.compareSync(plainPassword, hashedPassword);
  }
}
