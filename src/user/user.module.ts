import { ConfigModule, ConfigService } from '@nestjs/config';
import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { createConnection } from 'mysql2/promise';

const connectionFactory = {
  provide: 'CONNECTION',
  imports: [ConfigModule],
  inject: [ConfigService],
  useFactory: async (configService: ConfigService) => {
    const connection = await createConnection({
      host: configService.get('MYSQL_HOST'),
      port: configService.get('MYSQL_PORT'),
      user: configService.get('MYSQL_USER'),
      database: configService.get('MYSQL_DATABASE'),
      password: configService.get('MYSQL_PASS'),
      namedPlaceholders: true,
    });
    return connection;
  },
};

@Module({
  imports: [ConfigModule.forRoot()],
  controllers: [UserController],
  providers: [UserService, connectionFactory],
  exports: [UserService],
})
export class UserModule {}
