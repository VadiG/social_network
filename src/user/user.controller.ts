import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  UseGuards,
  Req,
} from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDto } from './dto/create_user.dto';
import { JwtAuthGuard } from '../auth/guard/jwt-auth.guard';
import { Public } from '../auth/guard/public.guard';
import { IRequest } from '../interface';

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Public()
  @Post('register')
  create(@Body() createUserDto: CreateUserDto) {
    return this.userService.create(createUserDto);
  }

  @Get()
  findAll() {
    return this.userService.findAll();
  }

  @Post('friend')
  addFriend(@Req() req: IRequest, @Body() friend: { id: number }) {
    return this.userService.addFriend(req.user.userId, friend.id);
  }

  @Get('friend')
  findAllFriends(@Req() req: IRequest) {
    return this.userService.findAllFriends(req.user.userId);
  }
}
