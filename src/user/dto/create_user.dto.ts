import { IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';
import { Transform } from 'class-transformer';
import * as bcrypt from 'bcryptjs';

export class CreateUserDto {
  @IsNotEmpty()
  @IsString()
  first_name: string;

  @IsOptional()
  @IsString()
  second_name = null;

  @IsNotEmpty()
  @IsString()
  birthday: string;

  @IsNotEmpty()
  @IsNumber()
  gender_id: number;

  @IsNotEmpty()
  @IsNumber()
  city_id: number;

  @IsOptional()
  @IsString()
  hobby = null;

  @IsNotEmpty()
  @IsString()
  login: string;

  @IsNotEmpty()
  @IsString()
  @Transform(({ value }) => bcrypt.hashSync(value, 10))
  password: string;
}
