export class FindUserDto {
  id: string;
  first_name: string;
  second_name: string;
  birthday: string;
  gender_id: number;
  city_id: number;
  hobby: string;
  password: string;
  login: string;
}

export class FindUserInfoDto {
  id: string;
  first_name: string;
  second_name: string;
  birthday: string;
  gender: string;
  city: string;
  hobby: string;
}
