import {
  HttpException,
  HttpStatus,
  Inject,
  Injectable,
  Logger,
} from '@nestjs/common';
import { CreateUserDto } from './dto/create_user.dto';
import { Connection } from 'mysql2/promise';
import { FindUserDto } from './dto/find_user.dto';
import {
  INSERT_FRIEND,
  INSERT_USER,
  SELECT_FRIEND_LIST,
  SELECT_USER,
  USER_BY_LOGIN,
} from './query';

@Injectable()
export class UserService {
  @Inject('CONNECTION')
  private readonly connection: Connection;

  async create(createUserDto: CreateUserDto) {
    try {
      const [rows] = await this.connection.execute(INSERT_USER, createUserDto);
      const { password, ...resp } = createUserDto;
      return { ...resp, id: rows['insertId'] };
    } catch (e) {
      Logger.error(e.message);
      let message = 'Произошла ошибка попробуйте еще раз.';
      if (e.message.includes('Duplicate entry')) {
        message = 'Такой логин уже существует';
      }
      throw new HttpException(message, HttpStatus.BAD_REQUEST);
    }
  }

  async addFriend(userId: number, friendId) {
    try {
      const [rows] = await this.connection.execute(INSERT_FRIEND, {
        userId,
        friendId,
      });
      return { id: rows['insertId'] };
    } catch (e) {
      Logger.error(e.message);
      let message = 'Произошла ошибка попробуйте еще раз.';
      if (e.message.includes('Duplicate entry')) {
        message = 'Пользователь уже добавлен';
      }
      throw new HttpException(message, HttpStatus.BAD_REQUEST);
    }
  }

  async findAll() {
    const [rows] = await this.connection.execute(SELECT_USER);
    return rows;
  }

  async findByLogin(login: string): Promise<FindUserDto> {
    const [rows] = await this.connection.execute(USER_BY_LOGIN, [login]);
    return rows[0];
  }

  async findAllFriends(userId: number) {
    const [rows] = await this.connection.execute(SELECT_FRIEND_LIST, {
      userId,
    });
    return rows;
  }
}
