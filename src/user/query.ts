export const SELECT_USER = `
    SELECT 
      user.id first_name, 
      second_name, 
      birthday, 
      gender.name AS gender, 
      city.name AS city, 
      hobby 
    FROM 
      user 
      LEFT JOIN city ON user.city_id = city.id 
      LEFT JOIN gender ON user.gender_id = gender.id
`;

export const SELECT_FRIEND_LIST = `
    ${SELECT_USER}
    INNER JOIN friend_list ON user.id = friend_list.friend_id 
    AND friend_list.user_id = :userId
`;

export const INSERT_USER = `
    INSERT INTO user (
      first_name, second_name, birthday, 
      gender_id, city_id, hobby, login, 
      password
    ) 
    VALUES 
      (
        :first_name, 
        :second_name, 
        STR_TO_DATE(:birthday, "%Y-%m-%d"), 
        :gender_id, 
        :city_id, 
        :hobby, 
        :login, 
        :password
      );
`;

export const INSERT_FRIEND = `
    INSERT INTO friend_list (
      user_id, friend_id
    ) 
    VALUES 
      (
        :userId, 
        :friendId
      );
`;

export const USER_BY_LOGIN = 'SELECT * FROM user WHERE login = ?;';
