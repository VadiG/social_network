export interface IRequest extends Request {
  user: {
    userId: number;
    first_name: string;
    second_name?: string;
  };
}
