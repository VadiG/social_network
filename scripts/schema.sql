CREATE TABLE IF NOT EXISTS city (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(30),
    PRIMARY KEY (id)
);


CREATE TABLE IF NOT EXISTS gender (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(30),
    description VARCHAR(200),
    PRIMARY KEY (id)
) DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

CREATE TABLE IF NOT EXISTS user (
	id INT NOT NULL AUTO_INCREMENT,
	first_name VARCHAR(30) NOT NULL,
	second_name VARCHAR(30),
	birthday DATE,
	gender_id INT,
	city_id INT,
	hobby MEDIUMTEXT,
	login VARCHAR(30) NOT NULL UNIQUE,
	password VARCHAR(100) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (city_id)
        REFERENCES city(id)
        ON DELETE CASCADE,
    FOREIGN KEY (gender_id)
        REFERENCES gender(id)
        ON DELETE CASCADE
);

CREATE TABLE friend_list(
	user_id INT,
	friend_id INT,
	FOREIGN KEY (user_id)
        REFERENCES user(id)
        ON DELETE CASCADE,
    FOREIGN KEY (friend_id)
        REFERENCES user(id)
        ON DELETE CASCADE,
    UNIQUE KEY `friend_list_uniq_id` (`user_id`,`friend_id`)
);